import Head from 'next/head';
import Layout from '../components/Layout';
import { RaisedTopNav, RaisedTopNavLink } from '../components/RaisedTopNav';
import TagSummary from '../components/TagSummary';
import ReleaseList from '../components/ReleaseList';
import StaffCredits from '../components/StaffCredits';
import VoteGraph from '../components/VoteGraph';
import Gallery from '../components/Gallery';
import vnData from '../data/vn';
import CharacterBrowser from '../components/CharacterBrowser';
import EntityCommaList from '../components/EntityCommaList';

export default function VnPage() {
  return (
    <Layout headerControls={<VnHeader vn={vnData} />}>
      <Head>
        <title>{vnData.title} | vndb</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="main-container container">
        <div className="vn-page">
          <div className="row">
            <div className="fixed-size-left-sidebar-md">
              <VnSidebar vn={vnData} />
            </div>
            <div className="col-md">
              <VnContent vn={vnData} />
            </div>
          </div>
          <VnCharacterBrowser vn={vnData} />
          <VnStats vn={vnData} />
        </div>
      </div>
    </Layout>
  );
}

function VnHeader({ vn }) {
  const languages = Object.values(vn.releaseData).map((x) => x.name);

  return (
    <div>
      <div className="row">
        <div className="fixed-size-left-sidebar-md"></div>
        <div className="col-md">
          <div className="vn-header">
            <img
              src={vn.coverImage}
              className="vn-header__img img img--rounded d-md-none"
            />
            <div className="vn-header__title">{vn.title}</div>
            {vn.originalTitle && (
              <div className="vn-header__original-title">
                {vn.originalTitle}
              </div>
            )}
            <div className="vn-header__details">
              {vn.voteScore}
              <div className="vn-header__sep"></div>
              {vn.lengthFormatted}
              <div className="vn-header__sep"></div>
              {languages.join(', ')}
            </div>
          </div>
          <RaisedTopNav>
            <RaisedTopNavLink href="#" active>
              Details
            </RaisedTopNavLink>
            <RaisedTopNavLink href="#">Discussions</RaisedTopNavLink>
            <RaisedTopNavLink href="#">Relations</RaisedTopNavLink>
            <RaisedTopNavLink href="#">History</RaisedTopNavLink>
            <RaisedTopNavLink href="#">Edit</RaisedTopNavLink>
          </RaisedTopNav>
        </div>
      </div>
    </div>
  );
}

function getUniqueReleaseEntity(releaseData, mapper) {
  const entities = []
    .concat(...Object.values(releaseData).map((x) => x.releases))
    .map(mapper);
  return entities.filter(
    (x, i) => entities.findIndex((y) => y.id == x.id) === i
  );
}

function VnSidebar({ vn }) {
  const developers = getUniqueReleaseEntity(vn.releaseData, (x) => x.developer);
  const publishers = getUniqueReleaseEntity(vn.releaseData, (x) => x.publisher);

  return (
    <div className="vn-page__top-details">
      <img
        src={vn.coverImage}
        className="img img--fit img--rounded elevation-1 d-none d-md-block"
      />
      <div className="add-to-list elevated-button elevation-1">
        <img src="/content/icons/plus.svg" className="svg-icon" />
        Add to my list
      </div>
      <dl className="vn-page__dl">
        <dt>Title</dt>
        <dd>{vn.title}</dd>
        <dt>Original Title</dt>
        <dd>{vn.originalTitle}</dd>
        <dt>Aliases</dt>
        <dd>{vn.aliases.join(', ')}</dd>
        <dt>Length</dt>
        <dd>{vn.lengthFormatted}</dd>
        <dt>{developers.length == 1 ? 'Developer' : 'Developers'}</dt>
        <dd>
          <EntityCommaList data={developers} link={(x) => '/p' + x.id} />
        </dd>
        <dt>{publishers.length == 1 ? 'Publisher' : 'Publishers'}</dt>
        <dd>
          <EntityCommaList data={publishers} link={(x) => '/p' + x.id} />
        </dd>
        {vn.shops && vn.shops.length > 0 && (
          <>
            <dt>Shops</dt>
            <dd>
              <ul>
                {vn.shops.map((shop, i) => (
                  <li key={i}>
                    <a href={shop.link}>
                      {shop.name} {shop.price}
                    </a>
                  </li>
                ))}
              </ul>
            </dd>
          </>
        )}
        {vn.relations && vn.relations.length > 0 && (
          <>
            <dt>Relations</dt>
            {vn.relations.map((x, i) => (
              <dd key={i}>
                <dl>
                  <dt>{x.name}</dt>
                  {x.vns.map((x) => (
                    <dd key={x.id}>
                      <a href={'/v' + x.id} title={x.originalTitle}>
                        {x.title}
                      </a>
                    </dd>
                  ))}
                </dl>
              </dd>
            ))}
          </>
        )}
      </dl>
    </div>
  );
}

function VnContent({ vn }) {
  return (
    <div className="stackaaaaa">
      <div
        className="description serif"
        id="about"
        dangerouslySetInnerHTML={{ __html: vn.descriptionHtml }}
      />
      <div className="section" id="tags">
        <TagSummary tags={vn.tags} />
      </div>
      <div className="section" id="releases">
        <h2 className="section__title">Releases</h2>
        <ReleaseList releases={vn.releaseData} />
        <a href={`/${vn.id}/releases`}>Compare releases</a>
      </div>
      <div className="section" id="staff">
        <h2 className="section__title">Staff</h2>
        <StaffCredits groups={vn.creditGroups} />
      </div>
      <div className="section gallery" id="gallery">
        <h2 className="section__title">
          Gallery
          <a
            className="switch gallery-r18-toggle"
            href="#"
            onClick={(e) => e.preventDefault()}
          >
            <div className="switch__label">18+</div>
            <div className="switch__toggle"></div>
          </a>
        </h2>
        <Gallery sections={vn.gallery} />
      </div>
    </div>
  );
}

function VnCharacterBrowser({ vn }) {
  return (
    <div className="section">
      <h2 className="section__title">Characters</h2>
      <CharacterBrowser characters={vn.characters} />
    </div>
  );
}

function VnStats({ vn }) {
  return (
    <div className="section stats">
      <h2 className="section__title">Stats</h2>
      <div className="row">
        <div className="stats__col col-md col-md-1">
          <h4 className="semi-muted">Vote distribution</h4>
          <VoteGraph stats={vn.voteStats} />
        </div>
        <div className="stats__col col-md col-md-1">
          <h4 className="semi-muted">Recent votes</h4>
          <div className="recent-votes">
            <table className="recent-votes__table tabular-nums">
              <tbody>
                {vn.recentVotes.map((vote, i) => (
                  <tr key={i}>
                    <td>
                      <a href={'/u' + vote.user.id}>{vote.user.name}</a>
                    </td>
                    <td>{vote.score}</td>
                    <td>{vote.date}</td>
                  </tr>
                ))}
              </tbody>
            </table>
            <div className="final-text">
              <a href="/v1/votes">All votes</a>
            </div>
          </div>
        </div>
        <div className="stats__col col-md col-md-1 semi-muted">
          <h4>Ranking</h4>
          <dl className="stats__ranking">
            <dt>Popularity</dt>
            <dd>ranked #89 with a score of 15.02</dd>
            <dt>Bayesian rating</dt>
            <dd>ranked #31 with a rating of 8.28</dd>
          </dl>
          <div className="final-text">
            <a href="/v/all">See best rated games</a>
          </div>
        </div>
      </div>
    </div>
  );
}
