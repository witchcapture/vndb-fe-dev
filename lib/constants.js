export const ListStatus = {
  All: -1,
  Unknown: 0,
  Playing: 1,
  Finished: 2,
  Stalled: 3,
  Dropped: 4,
};

export const ListStatusNames = {
  [ListStatus.All]: 'All',
  [ListStatus.Unknown]: 'Unknown',
  [ListStatus.Playing]: 'Playing',
  [ListStatus.Finished]: 'Finished',
  [ListStatus.Stalled]: 'Stalled',
  [ListStatus.Dropped]: 'Dropped',
};

export const ReleaseStatus = {
  Unknown: 0,
  Pending: 1,
  Obtained: 2,
  OnLoan: 3,
  Deleted: 4,
};

export const ReleaseStatusNames = {
  [ReleaseStatus.All]: 'All',
  [ReleaseStatus.Pending]: 'Pending',
  [ReleaseStatus.Obtained]: 'Obtained',
  [ReleaseStatus.OnLoan]: 'On loan',
  [ReleaseStatus.Deleted]: 'Deleted',
};
