export function classes(...args) {
  const arr = [];

  args.forEach((arg) => {
    if (typeof arg === 'object') {
      Object.entries(arg).forEach(([key, value]) => {
        if (value) arr.push(key);
      });
    } else if (arg) {
      arr.push(arg);
    }
  });

  return arr.join(' ');
}

export function unique(arr) {
  return arr.filter((x, i) => arr.indexOf(x) === i);
}

export function uniqueC(arr, comparator) {
  return arr.filter((x, i) => arr.findIndex((y) => comparator(x, y)) === i);
}
