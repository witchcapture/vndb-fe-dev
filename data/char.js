import vn from './vn';

const char = { ...vn.characters[0] };

char.vns = [{ vn: vn, role: 'Protagonist', voicedBy: char.voicedBy }];

char.otherInstances = [
  {
    id: 328,
    name: 'Name here',
    originalName: '山田 太郎',
    image: 'https://s2.vndb.org/ch/41/28541.jpg',
    gender: 'M',
    bloodType: 'O',
    vns: [
      { id: 90, title: 'Muv-Luv Photonmelodies♮' },
      { id: 91, title: 'Muv-Luv Extra' },
      { id: 92, title: 'Muv-Luv Alternative' },
    ],
    descriptionShortText:
      'An unemployed man, one of the game’s protagonists. When he' +
      'attempted suicide in Inokashira Park, he was found and rescued' +
      'by Tokisaka Yukari. After he’s discharged from the hospital,' +
      'he’s hired as a (temporary) assistant at the Tokisaka' +
      'Detective Agency.',
  },
  {
    id: 329,
    name: 'Name here',
    originalName: '山田 太郎',
    image: 'https://s2.vndb.org/ch/41/28541.jpg',
    gender: 'M',
    bloodType: 'O',
    vns: [
      { id: 90, title: 'Muv-Luv Photonmelodies♮' },
      { id: 91, title: 'Muv-Luv Extra' },
      { id: 92, title: 'Muv-Luv Alternative' },
    ],
    descriptionShortText:
      'An unemployed man, one of the game’s protagonists. When he' +
      'attempted suicide in Inokashira Park, he was found and rescued' +
      'by Tokisaka Yukari. After he’s discharged from the hospital,' +
      'he’s hired as a (temporary) assistant at the Tokisaka' +
      'Detective Agency.',
  },
];

export default char;
