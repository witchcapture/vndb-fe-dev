import { classes } from '../lib/util';

export default function Card({
  noSeparators,
  body,
  section,
  title,
  headerButtons,
  subheading,
  white,
  href,
  children,
}) {
  let headerEl = null;
  let subheaderEl = null;

  if (title || headerButtons) {
    headerEl = (
      <Card.Header>
        <Card.Title>{title}</Card.Title>
        {headerButtons && (
          <Card.HeaderButtons>{headerButtons}</Card.HeaderButtons>
        )}
      </Card.Header>
    );
  }

  if (subheading) {
    subheaderEl = <Card.Subheading>{subheading}</Card.Subheading>;
  }

  let WrapComponent;
  if (body) WrapComponent = Card.Body;
  if (section) WrapComponent = Card.Section;

  const Tag = href ? 'a' : 'div';

  return (
    <Tag
      className={classes('card', {
        'card--no-separators': noSeparators,
        'card--white': white,
      })}
      href={href}
    >
      {headerEl}
      {subheaderEl}
      {WrapComponent ? <WrapComponent>{children}</WrapComponent> : children}
    </Tag>
  );
}

Card.Body = function CardBody({ children }) {
  return <div className="card__body">{children}</div>;
};

Card.Section = function CardSection({ children }) {
  return <div className="card__section">{children}</div>;
};

Card.Header = function CardHeader({ children }) {
  return <div className="card__header">{children}</div>;
};

Card.Title = function CardTitle({ children }) {
  return <div className="card__title">{children}</div>;
};

Card.Subheading = function CardSubheading({ children }) {
  return <div className="card__subheading">{children}</div>;
};

Card.HeaderButtons = function CardHeaderButtons({ children }) {
  return <div className="card__header-buttons">{children}</div>;
};
