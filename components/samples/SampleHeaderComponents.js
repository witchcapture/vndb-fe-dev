export default function SampleHeaderComponents() {
  return (
    <div>
      <h2>Header components</h2>
      <p>
        These go inside .raised-top-container &gt; .raised-top &gt; .container
      </p>
      <h3>Detail Page Title</h3>
      <div className="example">
        <div className="detail-page-title">Detail Page Title</div>
      </div>
      <h3>Subnav</h3>
      <div className="example">
        <div className="nav raised-top-nav">
          <div className="nav__item">
            <a href="/u2" className="nav__link">
              Details
            </a>
          </div>
          <div className="nav__item--active nav__item">
            <a href="/u2/list" className="nav__link">
              List
            </a>
          </div>
          <div className="nav__item">
            <a href="/u2/wish" className="nav__link">
              Wishlist
            </a>
          </div>
          <div className="nav__item">
            <a href="/u2/posts" className="nav__link">
              Posts
            </a>
          </div>
          <div className="nav__item">
            <a href="/t/u2" className="nav__link">
              Discussions
            </a>
          </div>
          <div className="nav__item">
            <a href="/g/links?uid=2" className="nav__link">
              Tags
            </a>
          </div>
          <div className="nav__item">
            <a href="/u2/hist" className="nav__link">
              Contributions
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
