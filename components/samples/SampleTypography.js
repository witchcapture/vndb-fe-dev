export default function SampleTypography() {
  return (
    <div>
      <h2>Typography</h2>
      <div className="example">
        <h1>h1 header</h1>
        <h2>h2 header</h2>
        <h3>h3 header</h3>
        <h4>h4 header</h4>
        <h5>h5 header</h5>
        <p>Paragraph</p>
        <p className="serif">
          serif text <span className="spoiler">spoiler</span> foo
        </p>
        <p className="semi-muted">semi-muted</p>
        <p className="muted">muted</p>
        <a href="#">Link</a>
      </div>
    </div>
  );
}
