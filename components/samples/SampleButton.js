import Button from '../Button';
import ButtonGroup from '../ButtonGroup';
import MoreButton from '../MoreButton';

export default function SampleButton() {
  return (
    <div>
      <h2>Button</h2>
      <div className="example btn-set">
        <Button>Button</Button>
        <Button caret>With caret</Button>
        <Button>
          <img
            className="svg-icon opacity-muted"
            src="/content/icons/edit.svg"
          />
        </Button>
        <Button appearance="subtle">Subtle</Button>

        <ButtonGroup>
          <Button>One</Button>
          <Button>Two</Button>
        </ButtonGroup>

        <ButtonGroup>
          <Button active>Red</Button>
          <Button>Blue</Button>
        </ButtonGroup>

        <MoreButton />
        <MoreButton light />
      </div>
    </div>
  );
}
