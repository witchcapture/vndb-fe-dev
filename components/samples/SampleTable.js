export default function SampleTable() {
  return (
    <div>
      <h2>Table</h2>
      <div className="stack">
        <div>
          <h3>Standalone</h3>
          <div className="example">
            <table className="table">
              <thead>
                <tr>
                  <th width="15%">Date</th>
                  <th width="10%">Rev.</th>
                  <th width="75%">Entry</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td className="tabular-nums muted">2019-09-02</td>
                  <td>
                    <a href="/r65935.1">r65935.1</a>
                  </td>
                  <td>
                    <a href="/r65935.1">Khúc ca của Saya - HD Edition</a>
                  </td>
                </tr>
                <tr>
                  <td className="tabular-nums muted">2019-08-12</td>
                  <td>
                    <a href="/r45671.7">r45671.7</a>
                  </td>
                  <td>
                    <a href="/r45671.7">Kikokugai - The Cyber Slayer</a>
                  </td>
                </tr>
                <tr>
                  <td className="tabular-nums muted">2019-07-12</td>
                  <td>
                    <a href="/s10409.9">s10409.9</a>
                  </td>
                  <td>
                    <a href="/s10409.9">NIRAI-KANAI</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div>
          <h3>In a card</h3>
          <div className="example">
            <div className="card">
              <div className="card__header">
                <div className="card__title">Card</div>
              </div>
              <table className="table">
                <thead>
                  <tr>
                    <th width="15%">Date</th>
                    <th width="10%">Rev.</th>
                    <th width="75%">Entry</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="tabular-nums muted">2019-09-02</td>
                    <td>
                      <a href="/r65935.1">r65935.1</a>
                    </td>
                    <td>
                      <a href="/r65935.1">Khúc ca của Saya - HD Edition</a>
                    </td>
                  </tr>
                  <tr>
                    <td className="tabular-nums muted">2019-08-12</td>
                    <td>
                      <a href="/r45671.7">r45671.7</a>
                    </td>
                    <td>
                      <a href="/r45671.7">Kikokugai - The Cyber Slayer</a>
                    </td>
                  </tr>
                  <tr>
                    <td className="tabular-nums muted">2019-07-12</td>
                    <td>
                      <a href="/s10409.9">s10409.9</a>
                    </td>
                    <td>
                      <a href="/s10409.9">NIRAI-KANAI</a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div>
          <h3>Editable</h3>
          <div className="example">
            <div className="card card--no-separators">
              <div className="card__header">
                <div className="card__title">Card</div>
              </div>
              <table className="table table--responsive-single-sm">
                <thead>
                  <tr>
                    <th width="15%" className="th--nopad">
                      <a
                        className="with-sort-icon table-header with-sort-icon--down"
                        href="?o=a;p=1;s=date"
                      >
                        Date
                      </a>
                    </th>
                    <th width="40%" className="th--nopad">
                      <a
                        className="with-sort-icon--up with-sort-icon with-sort-icon--active table-header"
                        href="?o=d;p=1;s=title"
                      >
                        Title
                      </a>
                    </th>
                    <th width="10%" className="th--nopad">
                      <a
                        className="with-sort-icon--down table-header with-sort-icon"
                        href="?o=a;p=1;s=vote"
                      >
                        Vote
                      </a>
                    </th>
                    <th width="13%">Status</th>
                    <th width="7.33%"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td className="tabular-nums muted">2010-11-20</td>
                    <td>
                      <a
                        href="/v407"
                        title="C.D. Christmas Days ～サーカスディスク クリスマスデイズ～"
                      >
                        C.D. Christmas Days ~Circus Disc Christmas Days~
                      </a>
                    </td>
                    <td className="table-edit-overlay-base">
                      <input
                        type="text"
                        className="form-control form-control--table-edit form-control--stealth "
                        defaultValue="8"
                      />
                    </td>
                    <td className="table-edit-overlay-base">
                      <div>
                        Unknown
                        <select className="form-control form-control--table-edit form-control--table-edit-overlay">
                          <option value="0">Unknown</option>
                          <option value="1">Playing</option>
                          <option value="2">Finished</option>
                          <option value="3">Stalled</option>
                          <option value="4">Dropped</option>
                        </select>
                      </div>
                    </td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
              <div className="card__body">
                <div className="d-lg-flex jc-between align-items-center">
                  <div className="d-flex align-items-center" />
                  <div className="d-block d-lg-none mb-2" />
                  <div className="d-flex jc-right align-items-center">
                    <a href="?o=a;p=0;s=title" className="btn btn--disabled">
                      &lt; Prev
                    </a>
                    <div className="mx-3 semi-muted">page 1 of 1</div>
                    <a href="?o=a;p=2;s=title" className="btn btn--disabled">
                      Next &gt;
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
