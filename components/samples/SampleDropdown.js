import Button from '../Button';
import Dropdown from '../Dropdown';

export default function SampleDropdown() {
  const menu = [
    { text: 'Details', href: '/v97' },
    { text: 'History', href: '/v97/hist' },
    { text: 'Edit', href: '/v97/edit' },
    { text: 'Add release', href: '/v97/add' },
    { text: 'Add character', href: '/v97/addchar' },
  ];

  return (
    <div>
      <h2>Dropdown</h2>
      <div className="example btn-set">
        <Dropdown menu={menu}>
          <Button caret>Hello</Button>
        </Dropdown>

        <Dropdown menu={menu}>
          <Button caret>
            <img
              className="svg-icon opacity-muted"
              src="/content/icons/edit.svg"
            />
          </Button>
        </Dropdown>

        <Dropdown menu={menu}>
          <span>Plain trigger</span>
        </Dropdown>
      </div>
    </div>
  );
}
