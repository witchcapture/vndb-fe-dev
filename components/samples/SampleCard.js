import Button from '../Button';
import ButtonGroup from '../ButtonGroup';
import Card from '../Card';

export default function SampleCard() {
  return (
    <div className="stack stack--sm">
      <h2>Card</h2>
      <h3>Basic card</h3>
      <div className="example stack">
        <Card body>hello</Card>
        <Card>
          <Card.Body>Body</Card.Body>
          <Card.Section>Section</Card.Section>
          <Card.Body>Body</Card.Body>
          <Card.Section>Section</Card.Section>
        </Card>
      </div>

      <h3>Headers</h3>
      <div className="example stack">
        <Card title="Title" section>
          Section
        </Card>

        <Card
          title="Header with buttons"
          headerButtons={<Button>Detonate</Button>}
          section
        >
          Section
        </Card>

        <Card
          title="Header with toggle"
          headerButtons={
            <ButtonGroup>
              <Button active>
                <ListIcon />
              </Button>
              <Button>
                <GridIcon />
              </Button>
            </ButtonGroup>
          }
          section
        >
          Section
        </Card>

        <Card
          title="Header with subheading"
          subheading={
            <>
              For information, check the{' '}
              <a href="/d2#3" target="_blank">
                staff editing guidelines
              </a>
              . You can{' '}
              <a href="/s/new" target="_blank">
                create a new staff entry
              </a>{' '}
              if it is not in the database yet, but please{' '}
              <a href="/s/all" target="_blank">
                check for aliases first
              </a>
              .
            </>
          }
          section
        >
          Section
        </Card>
      </div>

      <h3>noSeparators</h3>
      <div className="example">
        <Card title="Title" noSeparators>
          <Card.Body>Body</Card.Body>
          <Card.Body>Body</Card.Body>
        </Card>
      </div>
    </div>
  );
}

function ListIcon() {
  return (
    <svg
      className="svg-icon"
      xmlns="http://www.w3.org/2000/svg"
      width="14"
      height="14"
      version="1"
    >
      <g fill="currentColor" fillRule="nonzero">
        <path d="M0 2h14v2H0zM0 6h14v2H0zM0 10h14v2H0z"></path>
      </g>
    </svg>
  );
}

function GridIcon() {
  return (
    <svg
      className="svg-icon"
      xmlns="http://www.w3.org/2000/svg"
      width="14"
      height="14"
      version="1"
    >
      <g fill="currentColor" fillRule="nonzero">
        <path d="M0 0h3v3H0zM0 5h3v3H0zM0 10h3v3H0zM5 0h3v3H5zM5 5h3v3H5zM5 10h3v3H5zM10 0h3v3h-3zM10 5h3v3h-3zM10 10h3v3h-3z"></path>
      </g>
    </svg>
  );
}
