import Gallery from '../Gallery';
import vn from '../../data/vn';

export default function SampleGallery() {
  return (
    <div>
      <h2>Gallery</h2>
      <div className="example">
        <Gallery sections={vn.gallery} />
      </div>
    </div>
  );
}
