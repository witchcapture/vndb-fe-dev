export default function SampleGrid() {
  return (
    <div>
      <h2>Grid</h2>
      <p>Similar to Bootstrap grid.</p>
      <div className="example">
        <div className="container">
          <div className="row">
            <div className="col col--1">Column</div>
            <div className="col col--2">Column</div>
          </div>
          <div className="row">
            <div className="col-md col-md--1">Column</div>
            <div className="col-md col-md--2">Column</div>
          </div>
        </div>
      </div>
    </div>
  );
}
