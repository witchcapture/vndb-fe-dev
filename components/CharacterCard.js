import { genderSymbol } from '../lib/formatting';

export default function CharacterCard({ char }) {
  return (
    <a className="card card--white character-card" href={'/c' + char.id}>
      <div className="character-card__left">
        <div className="character-card__image-container">
          <img className="character-card__image" src={char.image} />
        </div>
        <div className="character-card__main">
          <div className="character-card__name">
            {char.name} {genderSymbol(char.gender)} {char.bloodType}
          </div>
          <div className="character-card__sub-name">{char.originalName}</div>
          <div className="character-card__vns muted single-line">
            {char.vns.map((x) => x.title).join(', ')}
          </div>
        </div>
      </div>
      <div className="character-card__right serif semi-muted">
        {char.descriptionShortText}
      </div>
    </a>
  );
}
