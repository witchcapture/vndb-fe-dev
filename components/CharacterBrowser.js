import { useState } from 'react';
import { genderSymbol } from '../lib/formatting';
import { classes } from '../lib/util';
import CharacterStats from './CharacterStats';
import SpoilerOptions from './SpoilerOptions';
import Switch from './Switch';

export default function CharacterBrowser({ characters }) {
  const [activeCharacterId, setActiveCharacterId] = useState(
    characters[0] && characters[0].id
  );
  const [tagFilters, setTagFilters] = useState({
    spoilers: 0,
    sexual: false,
  });

  const activeCharacter = characters.find((x) => x.id === activeCharacterId);

  return (
    <div className="character-browser">
      <div className="row">
        <div className="fixed-size-left-sidebar-md">
          <CharacterBrowserSidebar
            characters={characters}
            activeCharacterId={activeCharacterId}
            onCharacterSelect={setActiveCharacterId}
            tagFilters={tagFilters}
            onTagFiltersChange={setTagFilters}
          />
        </div>
        <div className="col-md col-md--3 d-none d-md-block">
          {activeCharacter && (
            <CharacterInfo character={activeCharacter} filters={tagFilters} />
          )}
        </div>
      </div>
    </div>
  );
}

function CharacterBrowserSidebar({
  characters,
  activeCharacterId,
  onCharacterSelect,
  tagFilters,
  onTagFiltersChange,
}) {
  const [filterText, setFilterText] = useState();

  const filteredCharacters = characters.filter((char) => {
    return (
      !filterText || char.name.toLowerCase().includes(filterText.toLowerCase())
    );
  });

  const categories = [
    { id: 'protagonist', name: 'Protagonist', characters: [] },
    { id: 'main', name: 'Main characters', characters: [] },
    { id: 'side', name: 'Side characters', characters: [] },
  ];
  filteredCharacters.forEach((char) => {
    const category =
      categories.find((x) => x.id == char.category) ||
      categories.find((x) => x.id == 'main');
    category.characters.push(char);
  });

  const handleClick = (e, char) => {
    if (e.button === 0) {
      e.preventDefault();
      onCharacterSelect(char.id);
    }
  };

  return (
    <>
      <div>
        <input
          className="form-control"
          placeholder="Filter..."
          value={filterText}
          onChange={(e) => setFilterText(e.target.value)}
        />
      </div>
      <CharacterBrowserFilters
        tagFilters={tagFilters}
        onTagFiltersChange={onTagFiltersChange}
      />
      {categories.map((category) =>
        category.characters.length ? (
          <div key={category.id} className="character-browser__list">
            <div className="character-browser__list-title">{category.name}</div>
            {category.characters.map((char) => (
              <a
                key={char.id}
                className={classes(
                  'character-browser__char',
                  char.id == activeCharacterId &&
                    'character-browser__char--active'
                )}
                // href={'/c' + char.id}
                href={'/char'}
                onClick={(e) => handleClick(e, char)}
              >
                {char.name}
              </a>
            ))}
          </div>
        ) : null
      )}
    </>
  );
}

function CharacterBrowserFilters({ tagFilters, onTagFiltersChange }) {
  const updateFilters = (update) => {
    onTagFiltersChange({ ...tagFilters, ...update });
  };

  return (
    <div className="character-browser__top-items">
      <SpoilerOptions
        spoilers={tagFilters.spoilers}
        onChange={(spoilers) => updateFilters({ spoilers })}
      />
      <div>
        <Switch
          text="Sexual tags"
          checked={tagFilters.sexual}
          onToggle={() => updateFilters({ sexual: !tagFilters.sexual })}
        />
      </div>
      <div>
        <a href="/v1/chars">View all on one page</a>
      </div>
    </div>
  );
}

function CharacterInfo({ character, filters }) {
  const char = character;

  return (
    <div className="character">
      <div className="row">
        <div className="col-md">
          <div className="character__name">{char.name}</div>
          <div className="character__subtitle">
            {char.originalName +
              ' ' +
              genderSymbol(char.gender) +
              ' ' +
              char.bloodType}
          </div>
          <div className="character__description serif">
            <p dangerouslySetInnerHTML={{ __html: char.descriptionHtml }}></p>
          </div>
        </div>
        <div className="col-md character__image">
          <img src={char.image} className="img img--fit img--rounded" />
          <dl></dl>
        </div>
      </div>
      <div className="character__traits">
        <CharacterStats character={char} filters={filters} />
      </div>
    </div>
  );
}
