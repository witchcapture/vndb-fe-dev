import { useState, useEffect, useRef, useMemo } from 'react';
import { classes } from '../lib/util';
import { useViewportWidth } from '../lib/hooks';
import Switch from './Switch';
import SpoilerOptions from './SpoilerOptions';

const tagToggles = [
  { key: 'content', name: 'Content' },
  { key: 'sexual', name: 'Sexual' },
  { key: 'technical', name: 'Technical' },
];

export default function TagSummary({ tags }) {
  const [options, setOptions] = useState({
    content: true,
    sexual: false,
    technical: true,
  });
  const [spoilers, setSpoilers] = useState(0);
  const [isCollapsed, setIsCollapsed] = useState(true);
  const [collapsible, setCollapsible] = useState(true);

  const tagsRef = useRef();

  const displayedTags = useMemo(() => {
    const validTypes = [
      options.content && 'cont',
      options.sexual && 'ero',
      options.technical && 'tech',
    ].filter((x) => x);
    return tags.filter(
      (tag) => tag.spoil <= spoilers && validTypes.includes(tag.type)
    );
  }, [tags, options, spoilers]);

  const { width } = useViewportWidth();

  useEffect(() => {
    setCollapsible(tagsRef.current.scrollHeight > 50);
  }, [tagsRef, displayedTags, width]);

  function toggleOption(key) {
    setOptions({ ...options, [key]: !options[key] });
  }

  return (
    <div className="tag-summary" data-hide-types="ero" data-hide-spoil="2">
      <div
        className={classes('tag-summary__tags', {
          'tag-summary--collapsed': isCollapsed,
        })}
        ref={tagsRef}
      >
        {displayedTags.map((tag, i) => (
          <Tag key={i} {...tag} />
        ))}
      </div>
      <div className="tag-summary__options">
        <div className="tag-summary__options-left">
          {collapsible && (
            <a
              href="#"
              className="link--subtle tag-summary__show-all"
              onClick={(e) => {
                e.preventDefault();
                setIsCollapsed(!isCollapsed);
              }}
            >
              {isCollapsed ? (
                <span className="caret caret--pre"></span>
              ) : (
                <span className="caret caret--pre caret--up"></span>
              )}{' '}
              Show all tags
            </a>
          )}
        </div>
        <div className="tag-summary__options-right">
          <SpoilerOptions spoilers={spoilers} onChange={setSpoilers} />
          {tagToggles.map((toggle) => (
            <div key={toggle.key} className="tag-summary__option">
              <Switch
                checked={options[toggle.key]}
                text={toggle.name}
                onToggle={() => toggleOption(toggle.key)}
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

function Tag({ id, name, frac }) {
  return (
    <a
      href={'/g' + id}
      className="tag-summary__tag"
      style={{ '--tag-frac': frac }}
    >
      {name}
    </a>
  );
}
