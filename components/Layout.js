import React from 'react';
import GlobalNav from './GlobalNav';

export default function Layout({ headerControls, children }) {
  return (
    <>
      <div className="top-bar" id="top"></div>
      <div className="raised-top-container">
        <div className="raised-top">
          <div className="container">
            <GlobalNav />
            {headerControls}
          </div>
        </div>
      </div>
      <div className="page-container">
        {children}
        <div className="container">
          <div className="footer">
            <div className="row">
              <div className="col-md col-md--1">
                <div className="footer__logo">
                  <a href="/" className="link-subtle">
                    vndb
                  </a>
                </div>
              </div>
              <div className="col-md col-md--4">
                <div className="footer__nav">
                  <a href="/d7" className="link--subtle">
                    about us
                  </a>
                  <span className="footer__sep">&middot;</span>
                  <a href="irc://irc.synirc.net/vndb" className="link--subtle">
                    #vndb
                  </a>
                  <span className="footer__sep">&middot;</span>
                  <a href="mailto:contact@vndb.org" className="link--subtle">
                    contact@vndb.org
                  </a>
                  <span className="footer__sep">&middot;</span>
                  <a
                    href="https://code.blicky.net/yorhel/vndb/src/branch/v3"
                    className="link--subtle"
                  >
                    source
                  </a>
                  <span className="footer__sep">&middot;</span>
                  <a href="/v/rand" className="link--subtle footer__random">
                    random vn{' '}
                    <img className="svg-icon" src="/content/icons/random.svg" />
                  </a>
                  <span className="footer__sep">&middot;</span>
                  3.0-frontend-test
                </div>
                <div className="footer__quote">
                  <a href="/v17" className="link--subtle">
                    "Busy? BUSY?! What could possibly be more important than
                    strip-mahjong?!"
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
