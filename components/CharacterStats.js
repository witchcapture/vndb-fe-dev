import { Fragment } from 'react';
import EntityCommaList from './EntityCommaList';

export default function CharacterStats({ character: char, filters }) {
  return (
    <dl className="dl--horizontal">
      <CharacterMeasurements character={char} />
      {char.traitCategories.map((cat) => (
        <CharacterTraitCategory key={cat.id} category={cat} filters={filters} />
      ))}
      <dt>Voiced by</dt>
      <dd>
        <a href="/">Takahashi Ganaranai</a>
      </dd>
    </dl>
  );
}

function CharacterMeasurements({ character: char }) {
  let measurements = [];
  if (char.height != null) {
    measurements.push(`Height: ${char.height}`);
  }
  if (char.weight != null) {
    measurements.push(`Weight: ${char.weight}`);
  }
  if (measurements.length === 0) return null;
  return (
    <>
      <dt>Measurements</dt>
      <dd>{measurements.join(', ')}</dd>
    </>
  );
}

function CharacterTraitCategory({ category: cat, filters }) {
  const tags = cat.tags.filter((x) => {
    return (
      (!x.sexual || filters.sexual) && (!x.spoil || x.spoil <= filters.spoilers)
    );
  });

  if (tags.length === 0) {
    return null;
  }

  return (
    <>
      <dt>
        <a href={`/i${cat.id}`}>{cat.name}</a>
      </dt>
      <dd>
        <EntityCommaList data={tags} link={(tag) => `/i${tag.id}`} />
      </dd>
    </>
  );
}
