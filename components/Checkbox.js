export default function Checkbox({ checked, label, onChange }) {
  return (
    <label className="checkbox">
      <input type="checkbox" checked={checked} onChange={onChange} /> {label}
    </label>
  );
}
