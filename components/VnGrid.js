import Dropdown from './Dropdown';
import MoreButton from './MoreButton';

export default function VnGrid({ entries, onEntryEdit, onEntryRemove }) {
  const menu = (entry) => [
    { text: 'Edit', onClick: () => onEntryEdit(entry) },
    { text: 'Remove', onClick: () => onEntryRemove(entry) },
  ];

  return (
    <div className="vn-grid">
      {entries.map((entry) => (
        <div key={entry.id} className="vn-grid__item">
          <div
            className="vn-grid__item-bg"
            style={{
              backgroundImage: "url('" + entry.vn.coverImage + "')",
            }}
          />
          <div className="vn-grid__item-overlay">
            <a href={'/v' + entry.vn.id} className="vn-grid__item-link"></a>
            <div className="vn-grid__item-top">
              <Dropdown menu={menu(entry)}>
                <MoreButton />
              </Dropdown>
              <div className="vn-grid__item-rating"> {entry.vote}</div>
            </div>
            <div className="vn-grid__item-name">{entry.vn.title}</div>
          </div>
        </div>
      ))}
    </div>
  );
}
