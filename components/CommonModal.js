import {
  useContext,
  createContext,
  useLayoutEffect,
  useRef,
} from 'react';

const ModalContext = createContext({});

function getScrollbarWidth() {
  var scrollDiv = document.createElement('div');
  scrollDiv.className = 'modal-scrollbar-measure';
  document.body.appendChild(scrollDiv);
  var scrollbarWidth =
    scrollDiv.getBoundingClientRect().width - scrollDiv.clientWidth;
  document.body.removeChild(scrollDiv);
  return scrollbarWidth;
}

function documentHasScrollbar() {
  const rect = document.body.getBoundingClientRect();
  return rect.left + rect.right < window.innerWidth;
}

export default function CommonModal({ children, open, onClose }) {
  useLayoutEffect(() => {
    if (open) {
      if (documentHasScrollbar()) {
        document.body.style.paddingRight = getScrollbarWidth() + 'px';
      }
      document.body.classList.add('has-modal-open');
    } else {
      document.body.style.paddingRight = '';
      document.body.classList.remove('has-modal-open');
    }
  }, [open]);

  const context = {
    close: onClose,
  };

  const onCloseRef = useRef(null);

  const modalElRef = useRef(null);

  if (onCloseRef.current != onClose) {
    onCloseRef.current = onClose;
  }

  function handleMouseDown(e) {
    if (e.target != modalElRef.current) return;
    const listener = (e) => {
      document.removeEventListener('mouseup', listener);
      if (e.target != modalElRef.current) return;
      if (onCloseRef.current) {
        onCloseRef.current();
      }
    };
    document.addEventListener('mouseup', listener);
  }

  if (open) {
    return (
      <>
        <div className="modal-backdrop" />
        <div
          className="modal modal--open"
          onMouseDown={handleMouseDown}
          ref={modalElRef}
        >
          <div className="modal__content">
            <ModalContext.Provider value={context}>
              {children}
            </ModalContext.Provider>
          </div>
        </div>
      </>
    );
  } else {
    return null;
  }
}

CommonModal.Header = function CommonModalHeader({ title, closeButton }) {
  const context = useContext(ModalContext);

  return (
    <div className="modal__header">
      <div className="modal__title">{title}</div>
      <button
        type="button"
        className="modal__close"
        aria-label="Close"
        onClick={context.close}
      />
    </div>
  );
};

CommonModal.Body = function CommonModalBody({ children }) {
  const context = useContext(ModalContext);

  return <div className="modal__body">{children}</div>;
};

CommonModal.Footer = function CommonModalFooter({ buttons }) {
  const context = useContext(ModalContext);

  return (
    <div className="modal__footer">
      <div className="modal__footer-buttons">{buttons}</div>
    </div>
  );
};
