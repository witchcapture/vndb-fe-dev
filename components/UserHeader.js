import { RaisedTopNav } from './RaisedTopNav';

const userNav = [
  { id: 'details', href: '/user', text: 'Details' },
  { id: 'list', href: '/user/list', text: 'List' },
  { id: 'posts', href: '#', text: 'Posts' },
  { id: 'discussions', href: '#', text: 'Discussions' },
  { id: 'contributions', href: '#', text: 'Contributions' },
];

export default function UserHeader({ user, page }) {
  return (
    <div>
      <div className="detail-page-title">{user.username}</div>
      <RaisedTopNav items={userNav} active={page} />
    </div>
  );
}
