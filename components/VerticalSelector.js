import { classes } from '../lib/util';

export default function VerticalSelector({ items, selected, onChange }) {
  function handleItemClick(e, id) {
    e.preventDefault();
    onChange(id);
  }

  return (
    <div className="vertical-selector">
      {items.map((x) => (
        <a
          key={x.id}
          href="#"
          className={classes(
            'vertical-selector__item',
            selected == x.id && 'vertical-selector__item--active'
          )}
          onClick={(e) => handleItemClick(e, x.id)}
        >
          {x.text}
        </a>
      ))}
    </div>
  );
}
