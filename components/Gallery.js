import { Fragment, useCallback, useState } from 'react';
import Lightbox from './Lightbox';

export default function Gallery({ sections }) {
  const [openedImage, setOpenedImage] = useState(false);

  const lbImages =
    openedImage &&
    sections.reduce((acc, section) => {
      section.images.forEach((img) => {
        acc.push(img);
      });
      return acc;
    }, []);

  const close = useCallback(() => {
    setOpenedImage(false);
  }, []);

  const openLightbox = (e, img) => {
    e.preventDefault();
    setOpenedImage(img.id);
  };

  return (
    <div className="gallery">
      {sections.map((section, i) => (
        <Fragment key={i}>
          <div className="gallery__section-title">
            <span className="lang-badge">
              {section.release.lang.toUpperCase()}
            </span>{' '}
            {section.release.name}
          </div>
          <div className="gallery__section">
            {section.images.map((img, i) => (
              <a
                key={i}
                href={img.url}
                className="gallery__image-link"
                onClick={(e) => openLightbox(e, img)}
              >
                <img className="gallery__image" src={img.thumb} />
              </a>
            ))}
          </div>
        </Fragment>
      ))}
      <Lightbox
        isOpen={openedImage}
        images={lbImages}
        initialIndex={
          openedImage && lbImages.findIndex((x) => x.id == openedImage)
        }
        onClose={() => setOpenedImage(false)}
      />
    </div>
  );
}
