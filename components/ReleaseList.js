import { Fragment } from 'react';
import PlatformIcons from './PlatformIcons';

export default function ReleaseList({ releases }) {
  return (
    <div className="relsm">
      {releases.map((lang) => (
        <Fragment key={lang.lang}>
          <div className="relsm__language">
            <span className="lang-badge">{lang.lang.toUpperCase()}</span>{' '}
            {lang.name}
          </div>
          <div className="relsm__table">
            {lang.releases.map((rel) => (
              <ReleaseRow key={rel.id} release={rel} />
            ))}
          </div>
        </Fragment>
      ))}
    </div>
  );
}

function ReleaseRow({ release }) {
  return (
    <div className="relsm__rel">
      <div className="relsm__rel-col relsm__rel-date tabular-nums">
        {release.date}
      </div>
      <a className="relsm__rel-col relsm__rel-name" href={'/r' + release.id}>
        {release.name}
      </a>
      <div className="relsm__rel-col relsm__rel-platforms icon-set">
        <PlatformIcons platforms={release.platforms} />
      </div>
      <div className="relsm__rel-col relsm__rel-mylist">
        <img className="svg-icon" src="/content/icons/add-circle.svg" />
      </div>
      <div className="relsm__rel-col relsm__rel-link">
        <a href={release.url}>Link</a>
      </div>
    </div>
  );
}
