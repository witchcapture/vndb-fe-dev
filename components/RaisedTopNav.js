import { classes } from '../lib/util';

export function RaisedTopNav({ children, items, active }) {
  return (
    <div className="nav raised-top-nav">
      {items &&
        items.map((x) => (
          <RaisedTopNavLink key={x.id} href={x.href} active={x.id == active}>
            {x.text}
          </RaisedTopNavLink>
        ))}
      {children}
    </div>
  );
}

export function RaisedTopNavLink({ href, active, children }) {
  return (
    <div className={classes('nav__item', active && 'nav__item--active')}>
      <a href={href} className="nav__link">
        {children}
      </a>
    </div>
  );
}
